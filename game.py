from random import randint

# make a list of the months with the month's name
months = [
    "Janurary",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
]


# Ask the user for their name
user_name = input("Hi! What is your name? ")

for guess_number in range(1, 6):
# Use the random number generator function to guess a random month and year
    month = randint(0, 11)
    random_month = months[month]
    random_year = randint(1924, 2004)

    # Use the random month and year to guess their birth date
    # First guess
    print("Guess ", guess_number, " :", user_name, "were you born in", random_month, "/", random_year, "?")
    guess = input("yes or no? ")

    # Depending on whether the guess was right or not have different responses
    if guess == "yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have other things to do. Good bye.")
    else:
        print("Drat! Lemme try again!")
